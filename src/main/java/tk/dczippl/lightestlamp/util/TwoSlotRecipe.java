package tk.dczippl.lightestlamp.util;

import net.minecraft.item.ItemStack;

public class TwoSlotRecipe
{
    public ItemStack input;
    public ItemStack catalyst;
    public ItemStack output;
    public ItemStack extra_output;
}