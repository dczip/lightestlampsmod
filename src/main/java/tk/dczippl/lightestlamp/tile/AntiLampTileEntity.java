package tk.dczippl.lightestlamp.tile;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import tk.dczippl.lightestlamp.init.ModTileEntities;

public class AntiLampTileEntity extends TileEntity
{
    public AntiLampTileEntity()
    {
        super(ModTileEntities.ANTILAMP_TE);
    }
}
